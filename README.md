# Simple Scripts

Several simple, usually one-file-scripts.

### Simple loader
Simple loader contains two, differenct versions of HTML/CSS loaders.

1. **CSS loader** base on CSS+HTML. CSS part is marked as CSS loader and also required to copy is all from comment `/* spin it! */`.
Also required is to copy the html part.

2. **SVG loader** is SVG image+some CSS. Required to copy is all under "SVG type" comment AND previously mentioned `/* spin it! */` part.
And, of course, svg.

Both of them are customizable via **style|css** (colors, size, speed) as well as HTML part for sizes. NB! bottom script is not needed (it's only for buttons!).